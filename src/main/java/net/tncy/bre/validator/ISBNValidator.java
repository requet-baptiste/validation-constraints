package net.tncy.bre.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

//    private String isbn;

    @Override
    public void initialize(ISBN constraintAnnotation) {
        // Ici le validateur peut accéder aux attribut de l’annotation.
//        isbn = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        boolean valid = false;
        // Algorithme de validation du numéro ISBN

        char[] isbn = bookNumber.replaceAll("-", "").toCharArray();
        String regex = "^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(bookNumber);
        valid =  matcher.matches();

        return valid;
    }

}
