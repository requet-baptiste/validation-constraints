package net.tncy.bre.validator;

import org.junit.*;

import static org.junit.Assert.*;

public class ISBNValidatorTest {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validateWelldefinedISBN() {
        ISBNValidator isbnValidator = new ISBNValidator();
        assertTrue(isbnValidator.isValid("978-0-596-52068-7", null));
    }

    @Test
    public void unvalidateNotWelldefinedISBN() {
        ISBNValidator isbnValidator = new ISBNValidator();
        assertFalse(isbnValidator.isValid("978-1-44-909-aa", null));
    }
}
